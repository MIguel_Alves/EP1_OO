#include <iostream>
#include "game_object.hpp"

using namespace std;

Game_object::Game_object(){}

Game_object::Game_object(char sprite, int pos_x, int pos_y){
	setSprite(sprite);
	setPos_x(pos_x);
	setPos_y(pos_y);
}

Game_object::~Game_object(){}

int Game_object::getPos_x(){
	return pos_x;
}
void Game_object::setPos_x(int valor){
	this->pos_x = valor;
}
int Game_object::getPos_y(){
	return pos_y;
}
void Game_object::setPos_y(int valor){
	this->pos_y = valor;
}
char Game_object::getSprite(){
	return sprite;
}
void Game_object::setSprite(char sprite){
	this->sprite = sprite ; //'?'
}

