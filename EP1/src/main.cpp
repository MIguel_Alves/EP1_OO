#include <iostream>
#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <stdio_ext.h>
#include "mapa.hpp"
#include "game_object.hpp"
#include "player.hpp"
#include "trap.hpp"
#include "bonus.hpp"
#include "draw.hpp"
//#include "colisor.hpp"

using namespace std;



int main(){

	Map * mapa = new Map();
	mapa->setRange();

	Player * player = new Player('@', 3, 3, 1, 0, 1);

	Draw * draw = new Draw();

	Trap * trap1 = new Trap();
	

	//Colisor * colisor = new Colisor();
	

	char a = 'a';
	
	while(TRUE){
		initscr();
		clear();
		keypad(stdscr, TRUE);
		noecho();

		draw->drawPlayer(mapa,player);
		draw->drawMapa(mapa);


		player->movimento();

		refresh();
		endwin();
	}

	return 0;
}

