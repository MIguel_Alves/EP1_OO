#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include "trap.hpp"

using namespace std;

Trap::Trap(){}

Trap::Trap(char sprite, int posx, int posy, int damage){
	this->sprite = sprite;
	this->pos_x = posx;
	this->pos_y = posy;
	this->damage = damage;
}

void Trap::setPos_x(int valor){
	valor = random()%1+19;
	this->pos_x = valor;
}

void Trap::setPos_y(int valor){
	valor = random()%1+49;
	this->pos_y = valor;
}

void Trap::setSprite(char sprite){
	this->sprite = '*';
}

void Trap::setDamage(int damage){
	this->damage = damage;
}

/*char Trap::getSprite(){
	return sprite;
}

int Trap::getPos_x(){
	return pos_x;
}

int Trap::getPos_y(){
	return pos_y;
}*/

int Trap::getDamage(){
	return damage;
}

// void MataPlayer