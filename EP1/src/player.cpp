#include <iostream>
#include <string>
#include "player.hpp"
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>

using namespace std;

Player::Player(){}

Player::Player(char sprite, int posx, int posy, bool alive, int score, bool winner){
	this->sprite = sprite;
	this->pos_x = posx;
	this->pos_y = posy;
	this->alive = alive;
	this->score = score;
	this->winner = winner;
	//setLastX(posx);
	//setLastY(posy);
}

void Player::setLastX(int lastX){
	this->lastX = lastX;
}

void Player::setLastY(int lastY){
	this->lastY = lastY;
}

void Player::setPos_x(int valor){
	this->pos_x += valor;
}

void Player::setPos_y(int valor){
	this->pos_y += valor;
}

void Player::setSprite(char sprite){
	this->sprite = sprite;
}

void Player::setAlive(bool alive){
	this->alive = alive;
}

void Player::setScore(int score){
	this->score += score;
}

void Player::setWinner(bool winner){
	this->winner = winner;
}

int Player::getLastX(){
	return lastX;
}

int Player::getLastY(){
	return lastY;
}

bool Player::getAlive(){
	return this->alive;
}

int Player::getScore(){
	return score;
}

bool Player::getWinner(){
	return this->winner;
}

void Player::movimento(){

	this->setLastX(this->getPos_x() );
	this->setLastY(this->getPos_y() );

	char direcao = 'l';

	direcao = getch();

	if(direcao == 'w'){
		this->setPos_y(-1);
	} else if (direcao == 's'){
		this->setPos_y(1);
	} else if (direcao == 'a'){
		this->setPos_x(-1);
	} else if (direcao == 'd'){
		this->setPos_x(1);
	}

}

void Player::returnMove(){
	this->setPos_x(this->getLastX());
	this->setPos_y(this->getLastY());
}
