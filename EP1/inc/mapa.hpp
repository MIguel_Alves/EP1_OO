#ifndef MAPA_H
#define MAPA_H

#include <iostream>
#include <string>

class Map{

	protected:
		char range[20][50];
		char sprite;

	public:
		Map();
		
		void setRange();
		void getRange();
		void addElemento(char sprite, int posx, int posy);
		char returnMatriz( int posx, int posy);
		//char returnSprite(int posx, int posy);
};

#endif