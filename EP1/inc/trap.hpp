#ifndef TRAP_H
#define TRAP_H

#include <iostream>
#include <string>
#include "game_object.hpp"

class Trap : public Game_object {
protected:
	/*char sprite;
	int pos_x;
	int pos_y;*/
	//Todos esses serão herdados!
	int damage;
public:
	Trap();
	Trap(char sprite, int posx, int posy, int damage);

	void setPos_x(int valor);
	void setPos_y(int valor);
	void setSprite(char sprite);
	void setDamage(int damage);

	/*int getPos_x();
	int getPos_y();
	char getSprite();*/
	int getDamage();

};
#endif