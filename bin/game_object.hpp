#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include <iostream>
#include <string>

class Game_object{
protected:
	char sprite;
	int pos_x;
	int pos_y;

public:	
	Game_object();
	Game_object(char sprite, int pos_x, int pos_y);
	//virtual ~Game_object()=0;
	~Game_object();

	void setPos_x(int valor);
	void setPos_y(int valor);
	void setSprite(char sprite);

	int getPos_x();
	int getPos_y();
	char getSprite();

};
#endif