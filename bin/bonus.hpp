#ifndef BONUS_H
#define BONUS_H

#include "game_object.hpp"
#include <iostream>

using namespace std;

class Bonus:public Game_object {
protected:
	int score;
public:
	Bonus();
	~Bonus();
	void setScore(int score);
	int getScore();
	void setPos_x(int valor);
	void setPos_y(int valor);
	void setSprite(char sprite);
	/*int getPos_x();
	int getPos_y();*/

};	
#endif