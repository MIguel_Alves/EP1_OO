#ifndef DRAW_HPP
#define DRAW_HPP

#include <iostream>
#include <string>
#include "trap.hpp"

class Draw{
private:
	
public:
	Draw();

	void drawMapa(Map * map);
	void drawPlayer(Map * map, Player * player);
};

#endif

