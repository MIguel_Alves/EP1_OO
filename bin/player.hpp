#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <string>
#include "game_object.hpp"


class Player : public Game_object {
	protected:

		int lastX;
		int lastY;
		bool alive;
		int score;
		bool winner;

	public:
		Player();
		Player(char sprite, int posx, int posy, bool alive, int score, bool winner);

		void setPos_x(int valor);
		void setPos_y(int valor);
		void setSprite(char sprite);
		void setAlive(bool alive);
		void setScore(int score);
		void setWinner(bool winner);
		void setLastX(int lastX);
		void setLastY(int lastY);
		
		bool getAlive();
		int getScore();
		bool getWinner();
		int getLastX();
		int getLastY();

		void movimento();
		void returnMove();

};

#endif